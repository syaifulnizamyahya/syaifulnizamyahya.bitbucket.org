Conventions
===========
Syaiful Nizam Yahya <syaifulnizamyahya@gmail.com>
:toc: left
:numbered:
:website: http://syaifulnizamyahya.bitbucket.org/
:hardbreaks:
:sectnumlevels: 6
:sectids:
:sectanchors:
:imagesdir: ./images
:iconsdir: ./icons
:stylesdir: ./styles
:scriptsdir: ./js
:doctype: book
:encoding: utf-8
:lang: en

== Introduction
=== Purpose

Describe a way which something is usually done especially within a particular area or activity.

=== Scope

=== Definitions, acronyms and abbreviations

=== References

=== Overview

This document is segmented into specific topics. Hierarchical organization is generally ignored.

== Software development guidelines

http://www.matthewjmiller.net/files/cc2e_checklists.pdf[Code Complete, 2nd Ed. Checklists]

== Source control

Uses git for source control.

Service provided by Visual Studio Online. 

Project location:  https://ijam-mast.visualstudio.com/

Use http://nvie.com/posts/a-successful-git-branching-model/[gitflow] convention when managing branch.

Atlassian tutorial on managing branch: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

=== Directory structure

Use lower case for and folder name.

No spaces for folder name.

.MAST filesystem tree
[tree,file="filesystem-tree.png"]
--
#MAST
##documentation
###notes
###tmrnd
##wp1
##wp2
###android
###ios
###web
###windows10
--

documentation:: Stores project documentation. No source code documentation here.

notes:: A general purpose notes folder for convenient purposes. Useful when taking notes when doing study. Store your notes in a folder named after you. 
tmrnd:: Store any documentation that will be submitted to TMRND here. SRS, SDD, patents, presentation slides etc.

wp1:: Working Party 1 folder. Store official documents in the root of this folder. For in development/personal documentations/in progress stuff, create a new folder named after you and stored it there. 

.Working Party 1 filesystem tree example usage
[tree,file="wp1-filesystem-tree.png"]
--
#MAST
##wp1
###Guidelines.html
###ali
####Guidelines.adoc
--

wp2:: Working Party 2 folder. 

android:: Store project files here. Store source code documentation here.

ios:: Store project files here. Store source code documentation here.

web:: Store project files here. Store source code documentation here.

windows10:: Store project files here. Store source code documentation here.

== Source code documentation

Use doxygen to generate documentation.

Use JavaDoc style for code comments.

Reuseable function must be commented.

Make a guide on how to build the project from scratch.

Bear in mind the capabilities of Intellisense/code completion helper tools. Do not prefix variable type in variable names. 

.Guide on code documentations 
.. http://www.matthewjmiller.net/files/cc2e_checklists.pdf[Code Complete, 2nd Ed. Checklists]

== Creating guidelines for developers

A good guideline will successfully allow developers to follow the guidelines independently, without ever to refer to other person or the internet.

The motive of a guideline is to allow developer to *understand* the required steps than can be *programmed* to achieve the **objectives**. Do include the *justification* for steps/decisions made in the guidelines.

.Guidelines and steps/decision justifications.
====
Guidelines.

. Install Ferrari engine because Ferrari engine is the fastest.

.Engine and its top speed
[width="100%",options="header"]
|====================
| Engine | Top speed 

| Ferrari
| 200kmh 

| Mercedes
| 140kmh 

| BMW
| 160kmh 

| Audi
| 123kmh 

|====================

====

== Methodology

Use Agile Kanban methodology provided by Visual Studio Online.

Use the tool as needed.

=== Writing Epic, Features, User stories and Tasks

Epic, Features, User stories and Tasks are product backlog. 

.Product backlog purposes
.. as a repositor for the work to be done
.. facilitates prioritization of work


.Hierarchy
.. Epics
.. Features
.. User stories
.. Tasks

==== Epics

Epic is defined as a group of deliverables.

.MAST Epics
.. Wireless Analyzer for web browser
.. Wireless Analyzer for Android
.. Wireless Analyzer for iOS
.. Wireless Analyzer for Windows 10
.. Wireless Analyzer Admin website
.. Research
.. IEMT Windows 10
.. IEMT iOS
.. IEMT Android

==== Features

Features are services provided by the system that fulfill a user need.

Feature can be a group of stories that are related and deliver a package of functionality that end users would generally expect to get all at once.

.Feature as software feature
====
- Automatically configure Wi-Fi AP to use the best channel
- Automatically configure Wi-Fi AP to use the best spectrum
====

Features can be objectives of research which can be break down into multiple related User stories

.Feature as research objectives
====
- How to find the best wireless channel
- How to remotely configure wireless channel
====

==== User stories

User stories are requirement.

===== User stories template 1

****
As a <__type of user__>, I want <__some goal__>.
****

.User stories example
====
- As a user, I want to estimate the closing price of stock
- As a user, I want to generate a unique identifier for a transaction
- As a user, I want to change the text displayed on a kiosk
- As a user, I want to merge the data for duplicate transactions
====

===== User stories template 2

****
As a <__type of user__>, I want <__some goal__> so that <__some reason__>.
****

.User stories example
====
- As a user, I want to estimate the closing price of stock so that I can make purchasing decision
- As a user, I want to generate a unique identifier for a transaction so that I can easily identify transaction
- As a user, I want to change the text displayed on a kiosk so that I can personalized the kiosk
- As a user, I want to merge the data for duplicate transactions so that I can reduce total storage usage
====

===== User stories template 3

****
<__action__> the <__result__> <__by|for|of|to__> <__object__>
****

.User stories example
====
- Estimate the closing price of stock
- Generate a unique identifier for a transaction
- Change the text displayed on a kiosk
- Merge the data for duplicate transactions
====

==== Tasks

Describe single type of work.

.Tasks example
====
- Organize a meeting on 1 Feb 2015
- Format server and install Openstack
====

== Documentation

Use http://www.asciidocfx.com/[Asciidoc FX].

Asciidoc FX uses Asciidoctor implementation of Asciidoc markup. Use this http://asciidoctor.org/docs/[documentation] for reference.

Use source control for your documentations.

Generate HTML from Asciidoc FX to publish.

== UI Design

Only use UI library that supports responsive design. 

Use platform official convention when designing UI experience.

