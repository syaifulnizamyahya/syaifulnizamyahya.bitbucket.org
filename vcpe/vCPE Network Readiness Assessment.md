# Network Readiness Assessment

# Executive summary

This document describes the methodology used in assessing enterprise 10Mbps connectivity utilizing virtual customer premises equiepment (vCPE).

The objective of this testing is to verify that enterprise 10Mbps connectivity service that utilizes vCPE are 
- suitable for enterprise production deployment.
- suitable for use in Banking industries
- suitable for use in Oil and Gas industries
- suitable for use in University environment

This document describes how we test the following mandatory events:
 
- 10Mbps Ethernet baseline performance (IPv4)
- Banking industries baseline performance (IPv4)
- Oil and Gas industries baseline performance (IPv4)
- University environment baseline performance (IPv4)
 
# The test bed

This section discusses requirements of systems under test and introduces the test equipment to be used.

## Devices under test

Connection between 2 endpoints, server 1 and server 2 are to be tested.

*Diagram *

## Test instrument

### Packet generator

- https://en.wikipedia.org/wiki/Packet_generator

### Ostinator

# Test procedures

This section describes the test procedures. This document follows the template given in most methodology documents of the IETF’s benchmarking working group, describing the following for each test:

- the test objective(s)
- the configuration to be used
- the procedure to be used
- the test metrics to be recorded
- reporting requirements

## 10Mbps Ethernet baseline performance (IPv4)

### Objectives

- to determine the baseline performance of the connectivity

### Test bed configurations

### Test procedure

### Metrics

- bandwidth (Mbps), maximum rate of data transfer
- latency (ms), delay between the sender and the receiver
- jitter (), packet delay variation
- error rate, the number of corrupted bits expressed as a percentage or fraction of the total sent

### Reporting requirement

- test results

## Banking industries baseline performance (IPv4)

*compare network performance result with banking industries baseline performance*

### Objectives

### Test bed configurations

### Test procedure

### Metrics

### Reporting requirement

- test results

## Oil and Gas industries baseline performance (IPv4)

### Objectives

### Test bed configurations

### Test procedure

### Metrics

### Reporting requirement

- test results

## University environment baseline performance (IPv4)

### Objectives

### Test bed configurations

### Test procedure

### Metrics

### Reporting requirement

- test results
