:numbered:
:hardbreaks:
:sectnumlevels: 10
:sectids:
:sectanchors:
:imagesdir: ./images
:iconsdir: ./icons
:stylesdir: ./styles
:scriptsdir: ./js
:doctype: book
:encoding: utf-8
:lang: en

== Story Guidelines
=== Introductions
==== Purpose

- Describes the game exposition, argumentation, description and narration
- Tempur is multiplayer focus, rarely driven by narrative
- The main purpose of this document is to describe the background stage for Tempur

==== Scope
==== Overview

=== Story

==== High Level Narrative Summary

- concoct a brief high-level summary of the primary story. 
- Think of it as an elevator pitch: make it succinct and snappy. 
- This short piece is probably the only story document most of the team will ever read, so it should be clear and compelling. 

==== Major Locations / Levels. 

- describes the scope of the game's environments, 
- get a rough idea of how much is needed and how much is feasible
- helps identified important art asset or technologies required

==== A Detailed Story Outline. 

- exquisitely detailed story document, complete with scene descriptions and gameplay objectives
- The amount of detail in this doc will vary according to how much the story influences the design, but it should be as thorough as possible. 

==== Story Presentation Plan.

- How, exactly, is the game's story being told, and who is responsible for telling it? 
- Do you have pre-rendered cutscenes or in-engine cutscenes? 
- Who will be putting these scenes together? 
- Perhaps you have no cutscenes whatsoever, and would like to tell your story on-the-fly. Is this feasible? Possible?

==== Estimated Cut-Scene Breakdown. 

If your game does contain cutscenes or animated in-game sequences of any kind, it is crucial to estimate their number very early on to get a good sense of the work to come. If you have a detailed story outline, this should be easy. On tight projects it also helps to determine ahead of time what the expected intricacy and quality of each scene is so your teams can allocate their resources appropriately.

==== Characters. 

As you generate your detailed story arc, you'll need to make a clear list of the number of characters needed. Who are these people, and what roles do they play in both the narrative and the gameplay? Which are simple NPCs? Which are robust, interactive characters? Which are bosses? Mission givers? Shop keepers? Tutorial mentors? Et cetera.

The artists will be generating all character models and animations, and they'll want to know the scope as soon as possible. If you spring 15 new NPCs on your artists halfway through the project, they will shank you in the break room -- believe it. Getting the character scope nailed down early will also help you determine how much "incidental dialog" the game will require, for these throwaway lines frequently take up as much space in the script as the main story dialog. This is no trivial amount, so keep close track of it.

==== Sort Out Your Text Database. 

This can be a tedious task, but it is crucial to sort out your text pipeline very early, and get your tools up and running. The longer you wait, the more you will hate yourself. Some games have complex or esoteric text requirements -- non-linear conversation systems, for instance -- so it is critical that you organize your data cleanly and clearly.

Also, take a moment to decide how the script will be delivered. Not all writers are familiar with the esoteric architecture of your text database, so if your writer is delivering the script in Word or Final Draft, you're going to need a pipeline to handle its transfer.