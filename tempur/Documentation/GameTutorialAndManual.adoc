:numbered:
:hardbreaks:
:sectnumlevels: 10
:sectids:
:sectanchors:
:imagesdir: ./images
:iconsdir: ./icons
:stylesdir: ./styles
:scriptsdir: ./js
:doctype: book
:encoding: utf-8
:lang: en

== Game Tutorial and Manual
=== Introductions
==== Purpose

- how to play manual for player
- documentation for in game tutorial

==== Scope
==== Overview

=== Available game modes
Lists of game modes and its detail descriptions

=== Controlling the character
How to move the character
What can the player do with the character

=== In game element descriptions
Describes in game items such as powerups etc.

=== In game tutorial

Describe in this format

.Tutorial section no
.. Tutorial objectives
.. High level overview on how the tutorial approach the player
.. Tutorial steps
.. Tutorial objectives for each steps (if available)